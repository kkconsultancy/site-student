#SITE Student App Service #
Project Code:  SITE0001 

SITE Student App is part of SITE Communications.

## Use Cases ##

### Use Case Diagram ###
![usecases.png](https://bitbucket.org/kkconsultancy/site-student/raw/e4af1bdc6c64c1402cc1dd198a520238ddb1f35b/Student_App_Service_Usecase.jpg)

## User Stories ##
### 1.SITE0001-01: Login ###

* Actor: Student

* Pre Condition:

  1. Student should have a working mobile number which is already registered with databse.

* Input:

   1. Working mobile number.

* Sequence:

   1. Student opens the SITE Student app in android phone.
   2. Login page appears.
   3. Page indicates that the mobile number is yet to be activated.
   4. Page displays a message to check mobile and prompt for 
      OTP.
   5. Student enters the OTP in login page.
   6. Page displays success and opens desired service.

* Post Condition:

   1. Page displays success message and services page appears.

* Exceptions:

   1. Login page re-appears with appropriate message if student enters invalid mobile number.
   2. Login page re-appears with appropriate message if the mobile number is not registered with database.
   3. Login  page shows "resend" button, in case the OTP is not entered in the first 5 minutes after the OTP has been sent.
   4. Login page will not show any special message if the OTP is not reachable. It just prompts for OTP, as usual.

### 2.SITE0001-002: View Profile ###

*  Actor:Student

* Pre Condition:
   
  1. Student should have a working mobile number which is already registered with databse.

* Input:

      1.mobile number

* Sequence:

    1. Student clicks on profile 
    2. Page displays the profile of the student with student name,
       register number,address etc,..

* Post Condition:

   1. Page displays the profile of the student with student name,
      register number,address etc,..

* Exceptions:

   1. Not Applicable

### SITE0001-003: View External Marks ###

* Actor:Student

* Pre Condition:

   1. Student should have a working mobile number which is already registered with databse.

* Input:

   1. mobile number

* Sequence:

   1. Student clicks on Marks
   2. Selects Branch and Semester to view that marks
   3. Page displays with three options i.e,External,Mid,and 
      Classtest 
   4. Student clicks on external to view the External marks
   5. Page displays the final exam marks..

* Post Condition:

   1. Page displays the external exam marks of the student.

* Exceptions:

   1. Not Applicable

### SITE0001-004: View Mid Marks ###

* Actor:Student

* Pre Condition:

   1. Student should have a working mobile number which is already registered with databse.

* Input:

   1. mobile number

* Sequence:

   1. Student clicks on Marks
   2. Selects Branch and Semester to view that marks
   3. Page displays with three options i.e., External,Mid and
      classtest
   4. Student selects mid and again page displays with two
      options i.e., mid1 and mid2             
   5. Student clicks on mid1 to view mid1 marks and clicks on 
      mid2 to view mid2 marks
   6. Page displays the respective mid marks
.
* Post Condition:

   1. Page displays the Mid exam marks of the student.

* Exceptions:

   1. Not Applicable

### SITE0001-05: View Classtest Marks ###

* Actor:Student

* Pre Condition:

   1. Student should have a working mobile number which is already registered with databse.

* Input:

   1. mobile number

* Sequence:

   1. Student clicks on Marks
   2. Selects Branch and Semester to view that marks
   3. Page displays with three options i.e., External,Mid and
      classtest
   4. Student clicks on Classtest 
   5. Page displays with four options classtest1,classtest2,,
      classtest3 and classtest4
   6. Student selects one of the four options
   7. Page displays respective classtest marks.       .

* Post Condition:

   1. Page displays the Classtest marks of the student.

* Exceptions:

   1. Not Applicable

### SITE0001-06: View Attendance ###

* Actor:Student

* Pre Condition:

  1. Student should have a working mobile number which is already registered with databse

* Input:

   1. mobile number

* Sequence:
   1. Student clicks on Attendance
   2. Student selects the month
   3. Page displays the attendance percentage of that month
   4. Displays the attendance on every day on particular month
      with colored circles
   5. Red circle represents student  is absent on that     day.                             .

* Post Condition:

   1. Displays the attendance on every day on particular month
      with colored circles

* Exceptions:

   1. Not Applicable

### SITE0001-07: View Achievements ###

* Actor:Student

* Pre Condition:

   1. Student should have a working mobile number which is already registered with databse.

* Input:

   1. mobile number

* Sequence:

   1. Student clicks on Achievements
   2. Page displays with two options caled Certifications and 
      Academic Awards
   3. Certifications displays the achievements in online certification
      courses
   4. Academic awards are based on the academic score and 
      attendance

* Post Condition:

   1. Certifications displays the achievements in online certification
      courses
   2. Academic awards displays the awards in academics  and  
      attendance 

* Exceptions:

   1. Not Applicable

### SITE0001-08: Suggestions ###

* Actor:Student

* Pre Condition:

  1. Student should have a working mobile number which is already registered with databse.

* Input:

  1. mobile number

* Sequence:
  1. Student clicks on Suggestions
  2. Page displays with two textboxes called rating and suggestions
  3. Student gives rating  to the app
  4. Student writes suggestiong to the app

* Post Condition:

  1. Student gives rating  to the app
  2. Student writes suggestiong to the app 

* Exceptions:

  1.  Not Applicable

### SITE0001-09: Share ###

* Actor:Student

* Pre Condition:

  1.  Student should have a working mobile number which is already registered with databse.

* Input:

  1. mobile number

* Sequence:

  1. Student clicks on Share
  2. Page displays with list of Apps like Whatsapp,Instagram,
     Messenger,SHAREit etc,
  3. Student clicks on one of the above apps to share the app

* Post Condition:

  1. Page displays with list of Apps like Whatsapp,Instagram,
     Messenger,SHAREit etc

* Exceptions:

  1. Not Applicable

## Workflow ##

### Workflow Diagram ###
![Workflow.png]( https://bitbucket.org/kkconsultancy/site-student/raw/e4af1bdc6c64c1402cc1dd198a520238ddb1f35b/Workflow%20student%20app_service.jpg)
## Development ##

### Dependencies ###
  1. Android Studio
  2.Integrated with github

### How to run ###
  1.Open Android Studio
  2.Open the project
  3.Run the app



